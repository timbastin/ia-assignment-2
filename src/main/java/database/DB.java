package database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;

import com.mongodb.ServerAddress;
import dev.morphia.Datastore;
import dev.morphia.Morphia;

import java.util.ArrayList;
import java.util.List;

public class DB implements DBAccess {
    private final Datastore datastore;
    private static DB instance;

    public static DB getInstance() {
        if (DB.instance == null) {
            DB.instance = new DB();
        }
        return DB.instance;
    }

    private DB() {
        Morphia morphia = new Morphia();
        morphia.mapPackage("models");
        MongoClient mongo = new MongoClient(new MongoClientURI("mongodb://ia:secret@127.0.0.1:27017"));
        this.datastore = morphia.createDatastore(mongo, "ia");
        this.datastore.ensureIndexes();
    }

    @Override
    public Datastore getDatastore() {
        return this.datastore;
    }
}
