package database;


import dev.morphia.Datastore;

public interface DBAccess {
    Datastore getDatastore();
}
