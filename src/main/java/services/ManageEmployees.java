package services;

import models.EvaluationRecord;
import models.SalesMan;

import java.util.List;

public interface ManageEmployees {

    public void createSalesMan(SalesMan record);
    public SalesMan readSalesMan(int sid);
    public void update(SalesMan record);
    public void deleteSalesMan(SalesMan salesMan);
    public List<SalesMan> querySalesMan(String attribute, String key);

    public List<EvaluationRecord> readEvaluationRecords(int sid);
    public void addPerformanceRecord(EvaluationRecord record, int sid);

}
