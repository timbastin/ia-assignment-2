package services;

import database.DB;
import database.DBAccess;
import dev.morphia.query.Query;
import dev.morphia.query.UpdateOperations;
import models.EvaluationRecord;
import models.SalesMan;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class EmployeeService implements ManageEmployees {

    private final DBAccess dbAccess;

    public EmployeeService() {
        this.dbAccess = DB.getInstance();
    }

    public void createSalesMan(SalesMan record) {
        this.dbAccess.getDatastore().save(record);
    }

    @Override
    public void update(SalesMan record) {
        UpdateOperations<SalesMan> updateOperations = this.dbAccess.getDatastore().createUpdateOperations(SalesMan.class)
                .set("name", record.getName());
        this.dbAccess.getDatastore().update(this.findQuery(record.getSid()), updateOperations);
    }

    public void deletePerformanceRecord(EvaluationRecord record) {
        this.dbAccess.getDatastore().delete(record);
    }

    public void deleteSalesMan(SalesMan salesMan) {
        this.dbAccess.getDatastore().delete(salesMan);
    }

    public void addPerformanceRecord(EvaluationRecord record, int sid) {
        SalesMan reference = this.readSalesMan(sid);
        if (reference != null) {
            record.setSalesMan(reference);
        }
        this.dbAccess.getDatastore().save(record);
    }

    public SalesMan readSalesMan(int sid) {
        return this.findQuery(sid).first();
    }

    public List<SalesMan> querySalesMan(String attribute, String key) {
        return this.dbAccess.getDatastore().find(SalesMan.class).filter(
                // security issue due to query injection.
                attribute + " =", key
        ).find().toList();

    }

    public List<EvaluationRecord> readEvaluationRecords(int sid) {
        return this.dbAccess.getDatastore().find(EvaluationRecord.class)
                .filter("salesMan.sid =", sid)
                .find().toList();
    }

    private Query<SalesMan> findQuery(int sid) {
        return this.dbAccess.getDatastore().createQuery(SalesMan.class)
                .filter("sid =", sid);
    }
}
