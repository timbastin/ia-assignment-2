package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.EvaluationRecord;
import models.SalesMan;
import services.EmployeeService;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet("/")
public class EmployeeController extends HttpServlet {
    private EmployeeService employeeService;
    @Override
    public void init() throws ServletException {
        this.employeeService = new EmployeeService();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String[]> params = req.getParameterMap();
        PrintWriter writer = resp.getWriter();
        if (params.containsKey("attribute") && params.containsKey("key")) {
            writer.println(mapper.writeValueAsString(this.employeeService.querySalesMan(params.get("attribute")[0], params.get("key")[0])));
        } else if(params.containsKey("sid")) {
            writer.println(mapper.writeValueAsString(this.employeeService.readSalesMan(Integer.parseInt(params.get("sid")[0]))));
        } else {
            throw new Error("No filter parameters provided");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> params = req.getParameterMap();
        if (params.containsKey("sid")) {
            SalesMan salesMan = this.employeeService.readSalesMan(Integer.parseInt(params.get("sid")[0]));
            this.employeeService.deleteSalesMan(salesMan);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonObject obj = Json.createReader(req.getReader()).read().asJsonObject();
        SalesMan record = new SalesMan();
        record.setName(obj.getString("name"));
        record.setSid(obj.getInt("sid"));
        this.employeeService.createSalesMan(record);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }
}
