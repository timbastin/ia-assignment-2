package services;

import models.EvaluationRecord;
import models.SalesMan;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TestEmployeeService {
    private EmployeeService employeeService;
    private SalesMan salesMan;
    @Before
    public void setup() {
        // implicit creation test.
        // if creation fails, all tests will fail
        this.employeeService = new EmployeeService();
        this.salesMan = new SalesMan();
        this.salesMan.setName("Tamara");
        salesMan.setSid(23);
        this.employeeService.createSalesMan(salesMan);
    }

    @After
    public void tearDown() {
        this.employeeService.deleteSalesMan(this.salesMan);
    }
    @Test
    public void testDeletion() {
        this.employeeService.deleteSalesMan(salesMan);
        assertNull(this.employeeService.readSalesMan(23));
    }

    @Test
    public void testUpdate() {
        salesMan.setName("Tamxily");
        this.employeeService.update(salesMan);
        // make the assertions, that the salesman was created successfully.
        assertEquals(salesMan.getName(), this.employeeService.readSalesMan(23).getName());
        this.employeeService.deleteSalesMan(salesMan);
    }

    @Test
    public void testAddRecord() {
        EvaluationRecord record = new EvaluationRecord();
        record.setRating(3);
        record.setId(10);
        this.employeeService.addPerformanceRecord(record, this.salesMan.getSid());
        List<EvaluationRecord> evaluationRecords = this.employeeService.readEvaluationRecords(this.salesMan.getSid());
        assertEquals(record.getId(), evaluationRecords.get(0).getId());
        // delete the record afterwards.
        this.employeeService.deletePerformanceRecord(record);
    }

    @Test
    public void testDeleteRecord() {
        EvaluationRecord record = new EvaluationRecord();
        record.setRating(3);
        record.setId(10);
        this.employeeService.addPerformanceRecord(record, this.salesMan.getSid());
        List<EvaluationRecord> evaluationRecords = this.employeeService.readEvaluationRecords(this.salesMan.getSid());
        assertEquals(1, evaluationRecords.size());
        this.employeeService.deletePerformanceRecord(record);
        evaluationRecords = this.employeeService.readEvaluationRecords(this.salesMan.getSid());
        assertEquals(0, evaluationRecords.size());
    }
}
